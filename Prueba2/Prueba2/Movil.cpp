#include "stdafx.h"
#include "Movil.h"


Movil::Movil()
{
}

Movil::Movil(double _x, double _y) : x(_x),y(_y)
{
}

void Movil::setX(double a)
{
	x = a;
}

void Movil::setY(double b)
{
	y = b;
}

double Movil::getX()
{
	return x;
}

double Movil::getY()
{
	return y;
}


Movil::~Movil()
{
}

void Movil::simular(double inc_t)
{
	y = y + 3 * inc_t;
}

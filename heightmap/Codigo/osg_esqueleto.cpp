﻿#pragma once

#define WIN32_LEAN_AND_MEAN    // stops windows.h including winsock.h
#include <winsock2.h>

#include "stdafx.h"
#include "FuncionesAyuda.h"

#include <string>
#include <math.h>
#include <iostream>
#include <vector>
#include <array>
#include <iterator>

#include <osg/AutoTransform>
#include <osg/Billboard>
#include <osg/BlendFunc>
#include <osg/Depth>
#include <osg/Geode>
#include <osg/Geometry>
#include <osg/Group>
#include <osg/Light>
#include <osg/LightSource>
#include <osg/LineWidth>
#include <osg/Material>
#include <osg/MatrixTransform>
#include <osg/Node>
#include <osg/Notify>
#include <osg/PositionAttitudeTransform>
#include <osg/Projection>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/TexEnv>
#include <osg/TexGen>
#include <osg/TexMat>
#include <osg/Texture2D>
#include <osgText/Text>

#include <osgDB/ReadFile>
#include <osgDB/Registry>
#include <osgDB/WriteFile>

#include <osgGA/TrackballManipulator>

#include <osgShadow/ShadowedScene>
#include <osgShadow/ShadowVolume>
#include <osgShadow/ShadowTexture>
#include <osgShadow/ShadowMap>
#include <osgShadow/SoftShadowMap>
#include <osgShadow/ParallelSplitShadowMap>
#include <osgShadow/LightSpacePerspectiveShadowMap>
#include <osgShadow/StandardShadowMap>
#include <osgShadow/ViewDependentShadowMap>

#include <osgViewer/Viewer>


#define PI 3.14159265358979323846

class SimulacionKeyboardEvent: public osgGA::GUIEventHandler
{
protected:
public:
	SimulacionKeyboardEvent::SimulacionKeyboardEvent()
	{		
	};
	struct control_teclado
	{
		double x;
		double y;
		
	} tecla_pulsada;
	
	virtual bool SimulacionKeyboardEvent::handle(const osgGA::GUIEventAdapter& ea,osgGA::GUIActionAdapter&)
    {
	    int tecla = ea.getKey();
		int evento =ea.getEventType();
		int shift = ea.getModKeyMask();
		int button = ea.getButton();

        switch(evento)
        {
        case(osgGA::GUIEventAdapter::KEYDOWN):	//Evento pulsacion tecla
			break;
		//case (osgGA::GUIEventAdapter::PUSH):
		case(osgGA::GUIEventAdapter::KEYUP):	// Se deja de pulsar una tecla
			{
		case 'c':
			tecla_pulsada.x += 1;
			break;
		case 'v':
			tecla_pulsada.x -= 1;
			break;
			}
		default:
			break;
		}
		return 0;
	}
};

int main(int, char**)
{
	osgViewer::Viewer viewer;
	viewer.setUpViewInWindow(50, 50, 800, 600, 0);
	osg::Group * grupo = new osg::Group;


	SimulacionKeyboardEvent* KeyboardEvent = new SimulacionKeyboardEvent();

	viewer.addEventHandler(KeyboardEvent);

	osg::Geode* malla = CreaMalla(256, 1);											// Crea la malla del suelo
	AddTexture(malla, "..\\Data\\Texturas\\Heightmap.png", 0);		// Añade la textura a la malla
	AddTexture(malla, "..\\Data\\Texturas\\Heightmap_normals.png", 1);		// Añade la textura a la malla
	AddTexture(malla, "..\\Data\\Texturas\\Heightmap_rgb.png", 2);		// Añade la textura a la malla
	AddTexture(malla, "..\\Data\\Texturas\\cesped.dds", 3);		// Añade la textura a la malla
	AddTexture(malla, "..\\Data\\Texturas\\snow.dds", 4);		// Añade la textura a la malla
	AddTexture(malla, "..\\Data\\Texturas\\mountain.dds", 5);		// Añade la textura a la malla
	AddShader(malla, "..\\Data\\heightmap_sombras.vert", "..\\Data\\heightmap_sombras.frag");
	grupo->addChild(malla);															//Añade malla al grupo
	//----------------------------- Luces ----------------------------------
	
	osg::Light* myLight = new osg::Light;
	myLight->setLightNum(0);
	myLight->setPosition(osg::Vec4(1000.0, 0.0, 0.0, 0.0f));				// Posicion foco
	myLight->setAmbient(osg::Vec4(1.0f, 1.0f, 1.0f, 1.0f));				// Color luz ambiente
	myLight->setDiffuse(osg::Vec4(0.5f, 0.5f, 0.45f, 1.0f));				// Color luz difusa

	osg::LightSource* lightS = new osg::LightSource;
	lightS->setLight(myLight);
	lightS->setLocalStateSetModes(osg::StateAttribute::ON);

	osg::Group * grupo_luz = new osg::Group;
	grupo_luz->addChild(lightS);
	grupo->addChild(grupo_luz);

	////------------------------------ Sombras ------------------------------------	

	//osgShadow::ShadowedScene* sc = new  osgShadow::ShadowedScene;
	//osg::ref_ptr<osgShadow::LightSpacePerspectiveShadowMapVB> sm = new osgShadow::LightSpacePerspectiveShadowMapVB;

	//sc->setShadowTechnique(sm);

	//float minLightMargin = 10.f;
	//float maxFarPlane = 1000;
	//unsigned int texSize = 1024;
	//unsigned int baseTexUnit = 0;
	//unsigned int shadowTexUnit = 6;

	//texSize = 4096;
	//sm->setLight(myLight);
	//sm->setMinLightMargin(minLightMargin);
	//sm->setMaxFarPlane(maxFarPlane);
	//sm->setTextureSize(osg::Vec2s(texSize, texSize));
	//sm->setShadowTextureCoordIndex(shadowTexUnit);
	//sm->setShadowTextureUnit(shadowTexUnit);
	//sm->setBaseTextureCoordIndex(baseTexUnit);
	//sm->setBaseTextureUnit(baseTexUnit);

	//const int ReceivesShadowTraversalMask = 0x1;
	//const int CastsShadowTraversalMask = 0x2;

	//sc->setReceivesShadowTraversalMask(ReceivesShadowTraversalMask);
	//sc->setCastsShadowTraversalMask(CastsShadowTraversalMask);

	//grupo->addChild(sc);
	osgDB::writeNodeFile(*grupo, "borrar.osg");
	viewer.setSceneData(grupo);
	viewer.getCamera()->setComputeNearFarMode(osg::CullSettings::DO_NOT_COMPUTE_NEAR_FAR);

	viewer.realize();
	while (!viewer.done())
	{
		viewer.getCamera()->setViewMatrixAsLookAt(osg::Vec3d(0, 300, 300), osg::Vec3d(0, 0, 0), osg::Vec3d(0, 0, 1));
		viewer.frame();
	}


	return 0;

	}
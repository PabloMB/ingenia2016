uniform sampler2D texUnit0;	
uniform sampler2D texUnit1;
uniform sampler2D texUnit2;
uniform sampler2D texUnit3;
uniform sampler2D texUnit4;
uniform sampler2D texUnit5;

varying float height;
varying vec3 normal;

void main (void)
{
	vec3 n = normalize(normal);
	float NdotL;
	NdotL = max(dot(n, normalize(vec3(gl_LightSource[0].position))),0.0);
	vec4 color = texture2D(texUnit2, gl_TexCoord[0].st);
	vec4 color_final = color.r * texture2D(texUnit3, gl_TexCoord[0].st * 10) + color.g * texture2D(texUnit4, gl_TexCoord[0].st * 10) + color.b * texture2D(texUnit5, gl_TexCoord[0].st * 10);
	color_final.rgb= color_final.rgb*(0.4 + NdotL*0.6);
	gl_FragData[0] = color_final;
}
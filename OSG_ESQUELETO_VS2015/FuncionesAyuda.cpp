#include "stdafx.h"	
#include "FuncionesAyuda.h"

float obtenerAltura(float posX, float posY,osg::Image* imagen_heightmap, int num_puntos,double separacion_puntos,double altura_total)
{
	double tamMalla = num_puntos*separacion_puntos; 
	
	//double xGeom = (posX + (tamMalla/2))/(tamMalla-1);
	//double yGeom = (posY + (tamMalla/2))/(tamMalla-1);
	int x1 = (posX + (tamMalla / 2));
	if (x1 < 0)
		x1 -= 1;
	int x2 = x1 + 1;	
	
	int y1 = (posY + (tamMalla / 2));
	if (y1 < 0)
		y1 -= 1;
	int y2 = y1 + 1;

	x1 /= (tamMalla - 1);
	x2 /= (tamMalla - 1);
	y1 /= (tamMalla - 1);
	y2 /= (tamMalla - 1);


	double z1 = imagen_heightmap->getColor(osg::Vec2(x1, y1)).r()*altura_total;
	double z2 = imagen_heightmap->getColor(osg::Vec2(x2, y1)).r()*altura_total;
	double z3 = imagen_heightmap->getColor(osg::Vec2(x2, y2)).r()*altura_total;
	double z4 = imagen_heightmap->getColor(osg::Vec2(x1, y2)).r()*altura_total;

	return posZ;
}


osg::Geode* CreaMalla(int num_puntos,double separacion_puntos)
{
	osg::Geometry* geom = new osg::Geometry;
	osg::Geode* geode = new osg::Geode;
	geode->addDrawable(geom);
	
	osg::Vec3Array* v = new osg::Vec3Array;
	osg::Vec2Array* texcoords1 = new osg::Vec2Array(); //Vectores de coordenadas
	osg::Vec2Array* texcoords2 = new osg::Vec2Array();
	for( int y = -num_puntos/2; y < num_puntos/2; y++)
	{
		for( int x = -num_puntos/2; x < num_puntos/2; x++)
			{
				double a = double(x*separacion_puntos);
				double b =  double(y*separacion_puntos);
				v->push_back(osg::Vec3(a,b,0)); //Push_back es como la funci�n que a�ad�a al final de una lista
				texcoords1->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1))); //Algoritmo para traducir a coordenadas
				texcoords2->push_back(osg::Vec2(((float)(x+(num_puntos/2))/(num_puntos-1)),((float)(y+(num_puntos/2)))/(num_puntos-1)));
			}
		}
	


	geom->setVertexArray(v);
	geom->setTexCoordArray(0,texcoords1);
	geom->setTexCoordArray(1,texcoords2);


	osg::DrawElementsUInt* elements = new osg::DrawElementsUInt(osg::PrimitiveSet::QUADS);
	for(unsigned int y=0;y<num_puntos-1;y++)
	{
		for(unsigned int x=0;x<num_puntos-1;x++)
		{
			elements->push_back((y+1)*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x);
			elements->push_back(y*(num_puntos)+x+1);
			elements->push_back((y+1)*(num_puntos)+x+1);
		}
	}
	
	
	geom->addPrimitiveSet(elements);
	return geode;
}

void AddTexture(osg::Node* node, std::string nombre_imagen, int unidad_textura)
{
	const char *vinit[] = {"texUnit0","texUnit1","texUnit2","texUnit3","texUnit4","texUnit5","texUnit6","texUnit7"};
	osg::Uniform*	t = new osg::Uniform(vinit[unidad_textura],unidad_textura); //Creaci�n de uniforms, cuidado de no pisar estos al hacer uno manual


	node->getOrCreateStateSet()->addUniform(t);

	
	
	osg::Image* image = osgDB::readImageFile( nombre_imagen);	//Lectura imagen
	osg::Texture2D* textura = new osg::Texture2D(image);
	textura->setFilter(osg::Texture2D::MIN_FILTER,osg::Texture2D::LINEAR_MIPMAP_LINEAR);
	textura->setFilter(osg::Texture2D::MAG_FILTER,osg::Texture2D::LINEAR);
	textura->setWrap(osg::Texture2D::WRAP_S,osg::Texture2D::REPEAT); //Esto era CLAMP, temas de bordes, repasar llegado el momento, creo que por eso vuelve a subir fuera del heightmap
	textura->setWrap(osg::Texture2D::WRAP_T,osg::Texture2D::REPEAT);

	
	node->getOrCreateStateSet()->setTextureAttributeAndModes(unidad_textura,textura,osg::StateAttribute::ON | osg::StateAttribute::OVERRIDE | osg::StateAttribute::PROTECTED);
	
	osg::AlphaFunc* alphaFunc = new osg::AlphaFunc;
    alphaFunc->setFunction(osg::AlphaFunc::GEQUAL,0.05f);
	node->getOrCreateStateSet()->setAttributeAndModes( alphaFunc, osg::StateAttribute::ON );

}

void AddShader(osg::Node* node,std::string vertex_shader,std::string fragment_shader)

{
	osg::Program* ProgramObject = new osg::Program;

	osg::Shader* VertexObject =   new osg::Shader( osg::Shader::VERTEX );
	VertexObject->loadShaderSourceFromFile(vertex_shader);

	osg::Shader* FragmentObject = new osg::Shader( osg::Shader::FRAGMENT);
	FragmentObject->loadShaderSourceFromFile(fragment_shader);

	ProgramObject->addShader(FragmentObject) && ProgramObject->addShader(VertexObject);

	node->getOrCreateStateSet()->setAttributeAndModes(ProgramObject, osg::StateAttribute::ON | osg::StateAttribute::PROTECTED);

}

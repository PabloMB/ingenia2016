#include "humano.h"
#include "FuncionesAyuda.h"	

humano::humano(double a, double b, double c, double teta) : punto(a, b, c) {
	setDireccion(teta);
	avance = retroceso = giro_izq = giro_der =  salto = false;
	velz = 0;
}

/*void humano::girar(double teta)
{
	direccion += teta;
}

void humano::avanzar(double d)
{
	setX(getX() + d*cos(direccion));
	setY(getY() + d*sin(direccion));
}*/

void humano::setSalto(bool a) {
	if (!salto && a)
		setVelZ(6);
	salto = a;
}

void humano::actua(double var_t)
{
	if (giro_izq) {
		direccion += CTE_GIRO;
	}
	else if (giro_der) {
		direccion -= CTE_GIRO;
	}
	if (avance) {
		setX(getX() + CTE_AVANCE*cos(direccion));
		setY(getY() + CTE_AVANCE*sin(direccion));
		//setZ(obtenerAltura()
	}
	else if (retroceso) {
		setX(getX() - CTE_AVANCE*cos(direccion));
		setY(getY() - CTE_AVANCE*sin(direccion));
	}
	if (salto) {
		setVelZ( getVelZ() + G*var_t );
		setZ( getZ() + velz*var_t + 0.5*G*var_t*var_t);
		if (getZ() < 0) {
			setZ(0);
			setSalto(false);
		}
	}
}

void humano::setDireccion(double teta)
{
	direccion = teta;
}

double humano::getDireccion()
{
	return direccion;
}


#pragma once

class punto {
	private:
		double x, y, z;
	
	public:
		punto(double a, double b, double c);
		
		void setX(double a) { x = a; }
		void setY(double b) { y = b; }
		void setZ(double c) { z = c; }

		double getX() { return x; }
		double getY() { return y; }
		double getZ() { return z; }

};

// LeerXML.cpp: define el punto de entrada de la aplicación de consola.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

#include "rapidxml-1.13/rapidxml.hpp"

char* file_to_char_array(std::string file_name) {
	int num_characters = 0;
	std::ifstream myfile(file_name);
	
	if (myfile.is_open())
	{
		char c;
		while(myfile.get(c))
		{
			num_characters++;
			//std::cout << c;
		}
		myfile.clear();
		myfile.seekg(0, myfile.beg); //para leer el archivo otra vez desde el principio
		char* array = new char[num_characters + 1];
		int i = 0;
		while (myfile.get(c))
		{
			array[i] = c;
			//std::cout << c;
			i++;
		}
		array[i] = '\0';
		myfile.close();
		return array;
	}
	else std::cout << "Unable to open file";

	return NULL;
}

int main()
{
	//pasa el archivo de texto a un array de caracteres
	char* a = file_to_char_array("trees.xml");
	
	//imprime la cadena obtenida
	std::cout << "Cadena obtenida:\n\n"; 
	int i = 0;
	while (a[i]!='\0') {
		std::cout << a[i];
		i++;
	}
	std::cout << std::endl << std::endl;

	{
		using namespace rapidxml;
		xml_document<> doc;    // character type defaults to char
		doc.parse<0>(a);    // 0 means default parse flags
		xml_node<> *nodo0 = doc.first_node();
		std::cout << "nodo padre (" << nodo0->name() << ") tiene:\n";
		int i = 1;
		for (xml_node<> *nodo1 = nodo0->first_node(); nodo1; nodo1 = nodo1->next_sibling()) {
			std::cout << "\tnodo " << i << " (" << nodo1->name() << ") tiene:\n";
			for (xml_node<> *nodo2 = nodo1->first_node(); nodo2; nodo2 = nodo2->next_sibling())
			{
				std::cout << "\t\t" << nodo2->name() << "=" << nodo2->value() << "\n";
			}
			i++;
		}
	}

	while(1){}

    return 0;
}

